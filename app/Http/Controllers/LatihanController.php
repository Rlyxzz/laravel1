<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LatihanController extends Controller
{
    public function perkenalan($nama,$alamat,$umur)
    {
        $nama1 = $nama;
        $alamat1 = $alamat;
        $umur1 = $umur;

        return view('pages.perkenalan', compact('nama1', 'alamat1', 'umur1'));
    }
    public function siswa()
    {
        $a = [
            array('id' => 1, 'name' => 'dadang', 'age' => 15, 'hobi' => [
                'mancing', 'futsal', 'renang'
            ],'mapel' =>[
                'bahasa indonesia', 'mtk',
            ]),
            array('id' => 2, 'name' => 'dudung', 'age' => 18, 'hobi' => [
                'baca buku', 'menyanyi'
            ],'mapel' =>[
                'mapel1' => 'bahasa inggris', 'mapel2' => 'mtk',
            ]),
        ];
        //dd($a);
        return view('pages.siswa', ['siswa' => $a]);
    }
    public function dosen()
    {
        $mahasiswa= [
            
            array('id' => 1, 'namad' => 'Yusuf Bachtiar', 'matkul' => 'Pemrongraman Web' ,
            'mahasiswa' => [
                ['nama' => 'Muhammad Soleh', 'nilai' =>78],
                ['nama' => 'Jujun Junaedi', 'nilai' =>85],
                ['nama' => 'Mamat Alkatiri', 'nilai' =>90],
            ]),
            array('id' => 2, 'namad' => 'Ya', 'matkul' => 'Pemrongraman Mobile' ,
            'mahasiswa' => [
                ['nama' => 'Alfred McTomimay', 'nilai' =>67],
                ['nama' => 'Bruno Kasmir', 'nilai' =>90],
            ]),
        ];
        //dd($a);
        return view('pages.dosen', ['dosen' => $mahasiswa]);
    }
    function shop(){
        $belanja = [
            ['nama_orang' => 'ALFIAN' ,
            'pembelian' =>
            [
                ['jenis' => 'SEPATU', 'merk' => 'VANS', 'harga' => 250000],
                ['jenis' => 'BAJU', 'merk' => 'ERIGO', 'harga' => 100000],
                ['jenis' => 'CELANA', 'merk' => 'VANS', 'harga' => 250000],
                ['jenis' => 'KUPLUK', 'merk' => 'VANS', 'harga' => 250000],
            ]],
            ['nama_orang' => 'DIDA',
            'pembelian' => 
            [
                ['jenis' => 'TOPI', 'merk' => 'EDGER', 'harga' => 100000],
                ['jenis' => 'BAJU', 'merk' => 'ERIGO', 'harga' => 75000],
                ['jenis' => 'CELANA', 'merk' => 'VANS', 'harga' => 125000],
            ]
            ]
            ];
            return view('pages.shop',['belanja' => $belanja]);
    }
    function televisi(){
    $tv = [
        ['nama_channel' => 'TVRI',
    'jadwal' => [
        ['nama' => 'TOP SPORT', 'jam_tayang' => "24.00 WIB"]
        ]
    ],
    ['nama_channel' => 'TVRI',
    'jadwal' => [
        ['nama' => 'TOP SPORT', 'jam_tayang' => "24.00 WIB"]
        ]
    ],
        ['nama_channel' => 'GTV',
        'jadwal' => [
        ['nama' => 'NARUTO', 'jam_tayang' => "18.00 WIB"]
        ]
    ],
        ['nama_channel' => 'TRANS 7',
        'jadwal' => [
        ['nama' => 'JEJAK SI GUNDUL', 'jam_tayang' => "13.00 WIB"]
        ]
    ],
        ['nama_channel' => 'TRANS TV',
        'jadwal' => [
        ['nama' => 'BIG MOVIES', 'jam_tayang' => "20.00 WIB"]
        ]
    ],
];
return view('pages.televisi' , ['televisi' => $tv]);
    }
}


