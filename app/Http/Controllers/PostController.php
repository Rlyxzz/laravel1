<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Post;

class PostController extends Controller
{
    public function tampil()
    {
        $post = Post::all();
        return view('post.index', compact('post'));
    }
}
