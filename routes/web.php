<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LatihanController;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// route basic 
Route::get('/about', function (){
    return view('tentang');
});
Route::get('/profile', function (){
    $nama = "Gibral";
    return view('pages.profile', compact('nama'));
});

// route parameter
Route::get('order/{makanan}/{minuman}/{harga}', function ($makanan, $minuman, $harga){
    return view('pages.order', compact('makanan', 'minuman', 'harga'));
});

Route::get('/biodata', function (){
    $nama = "M Gibral S";
    $kelas = "XII RPL 1";
    $umur = "17 Tahun";
    $tgl = "24 April 2005";
    $jk = "Laki Laki";
    return view('pages.biodata', compact('nama','kelas','umur','tgl','jk'));
});

// route optional parameter
Route::get('pesan/{menu?}', function ($a = "-"){
    return view('pages.pesan', compact('a'));
});
Route::get('latihan/{mkn?}/{mnm?}/{cmilan?}', function ($mkn = "Silahkan Pesan",$mnm = null ,$cmilan = null){
    
    return view('pages.latihan', compact('mkn','mnm','cmilan'));
});
Route:: get('perkenalan/{nama?}/{alamat?}/{umur?}', [LatihanController::class, 'perkenalan']);
Route:: get('siswa',[LatihanController::class, 'siswa']);
Route:: get('dosen',[LatihanController::class, 'dosen']);
Route:: get('shop',[LatihanController::class, 'shop']);
Route:: get('televisi',[LatihanController::class, 'televisi']);
// route post
Route::get('post',[PostController::class, 'tampil']);
