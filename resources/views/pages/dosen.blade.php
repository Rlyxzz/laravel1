<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <fieldset>
        <legend>
            Data Mahasiswa
        </legend>
        @foreach($dosen as $dsn)
            Nama : {{ $dsn['namad']}} <br>
            Mata Kuliah : {{$dsn['matkul']}}
            <?php $nilai_b = 0 ?>
            @foreach($dsn['mahasiswa'] as $mahasiswa)
                <li>Nama Mahasiswa : {{$mahasiswa['nama']}}</li>
                <li>Nilai : {{$mahasiswa['nilai']}}</li>
                <?php $nilai_b += $mahasiswa['nilai'] ?>
            @endforeach
            NILAI = {{$nilai_b  / count($dsn['mahasiswa'])}} <br>
            <hr>
        @endforeach
    </fieldset>
</body>
</html>